This project is developed as two separate projects.
On the src directory there will be the fe directory and the be directory. They follow distinct rules, check each readme to understand their characteristics.

To install, run aux/setup.sh [OPTION]. You can provide systemd or upstart as the type of install. Then you are able to start it as a service, named as hub1. 
It will also be installed as an application under hub1.

You will need mongodb and io.js to run it. The server instance may be setup to a different location, instructions can be found in the be directory. 

