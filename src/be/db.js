var fs = require('fs');

var cachedHubs;
var cachedThreads;
var cachedThreadSpam;
var cachedCommentSpam;
var cachedHubSpam;

var indexesSet = 0;

var maxIndexesSet = 4;

exports.commentSpam = function() {
  return cachedCommentSpam;
};

exports.threadSpam = function() {
  return cachedThreadSpam;
};

exports.hubSpam= function() {
  return cachedHubSpam;
};

exports.hubs = function() {
  return cachedHubs;
};

exports.threads = function() {
  return cachedThreads;
};

function indexSet(callback) {
  indexesSet++;

  if (indexesSet === maxIndexesSet) {
    callback();
  }
}

function initThreadSpamControl(db, callback) {

  cachedThreadSpam = db.collection('threadFloodControl');

  cachedThreadSpam.ensureIndex({
    expiration : 1
  }, {
    expireAfterSeconds : 0
  }, function setIndex(error, index) {
    if (error) {
      callback(error);
    } else {
      indexSet(callback);
    }
  });

}

function initCommentSpamControl(db, callback) {

  cachedCommentSpam = db.collection('commentFloodControl');

  cachedCommentSpam.ensureIndex({
    expiration : 1
  }, {
    expireAfterSeconds : 0
  }, function setIndex(error, index) {
    if (error) {
      callback(error);
    } else {
      indexSet(callback);
    }
  });

}

function initHubSpamControl(db, callback) {

  cachedHubSpam = db.collection('hubFloodControl');

  cachedHubSpam.ensureIndex({
    expiration : 1
  }, {
    expireAfterSeconds : 0
  }, function setIndex(error, index) {
    if (error) {
      callback(error);
    } else {
      indexSet(callback);
    }
  });

}

function initHubs(db, callback) {

  cachedHubs = db.collection('hubs');

  cachedHubs.ensureIndex({
    hubName : 1
  }, {
    unique : true
  }, function setIndex(error, index) {
    if (error) {
      callback(error);
    } else {
      indexSet(callback);
    }
  });

}

function initThreads(db, callback) {

  cachedThreads = db.collection('threads');

  cachedThreads.ensureIndex({
    hubName : 1,
    threadId : 1
  }, {
    unique : true
  }, function setIndex(error, index) {
    if (error) {
      callback(error);
    } else {
      indexSet(callback);
    }
  });

}

function checkCollections(db, callback) {

  initCommentSpamControl(db, callback);

  initThreadSpamControl(db, callback);

  initHubSpamControl(db, callback);


  initHubs(db, callback);

  initThreads(db, callback);

}

exports.init = function(callback) {

  try {
    var dbSettings = JSON
        .parse(fs.readFileSync(__dirname + '/dbSettings.json'));
  } catch (error) {
    callback(error);
    return;
  }

  var client = require('mongodb').MongoClient;

  var connectString = 'mongodb://';

  if (dbSettings.user) {
    connectString += dbSettings.user + ':' + dbSettings.password + '@';
  }

  connectString += dbSettings.address + ':';
  connectString += dbSettings.port + '/' + dbSettings.db;

  client.connect(connectString, function connectedDb(error, db) {

    if (error) {
      callback(error);
    } else {
      checkCollections(db, callback);
    }

  });

};
