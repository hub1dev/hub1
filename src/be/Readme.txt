This is a nodeclipse project, it has formatting and lint rules set up.

Coding standard: https://github.com/felixge/node-style-guide

Required modules:
mongodb. Latest version.

You can install the needed modules by running "npm install", there is a package.json file with the npm rules.

There will be two required files to be created on this directory:

settings.json: contains general settings, must contain the following values:
port: port where the server will listen to http. Defaults to 8080.
address: address where the server will listen to http. Defaults to 127.0.0.1.
ssl(Boolean): indicates if the server should also listen to https on port 443.
verbose(Boolean): indicates if the server should output everything that receives and outputs.
disable304(Boolean): indicates if the server will never return a code 304.
decayTick(Number): amount of minutes between each score decay.
scoreTreshold : lowest score after which a thread is deleted (calculated by hot-ranking algorithm sysOps->calculateScore)
threadCreationDelay(Number): amount of minutes between each thread allows an user to post new threads.
commentCreationDelay(Number): amount of minutes between each thread allows an user to post new comments.
decayMultiplier(Number): number to multiply the score of content on each decay tick.
minimumThreadCount(Number): how many threads should be kept no matter how low their score it.
topThreadsCount (Number): how many threads should be returned on the topThreads page.

Booleans defaults to false.

If you wish to use SSL, you must add both the key and pem files under the names "server.key" and "server.pem" on this directory.

dbSettings: contains the connection data to the server, must contain the following values:
address: address of the database.
port: port of the database.
db: database to be used.
user: user to be authenticated.
password: password of said user.

If no user is provided, then it will not try to authenticate.

To run on port 80 without sudo, use the command
sudo setcap 'cap_net_bind_service=+ep' `which iojs`



