#!/usr/local/bin/iojs

// common objects to the main thread and forks
var cluster = require('cluster');
var fs = require('fs');
var sysOps = require('./sysOps');

try {
  var settings = JSON.parse(fs.readFileSync(__dirname + '/settings.json'));

} catch (error) {
  console.log(error);
  console.log('Could not load settings.');
  return;
}

// functions
function startListening() {
  booted = true;

  if (settings.ssl) {
    try {

      var options = {
        key : fs.readFileSync('server.key'),
        cert : fs.readFileSync('server.pem')
      };

      require('https').createServer(options, function(req, res) {
        main(req, res);
      }).listen(443, address);

    } catch (error) {
      console.log(error);
      console.log('Could not listen to SSL.');
    }

  }

  require('http').createServer(function(req, res) {
    main(req, res);

  }).listen(port, address);

  var message = 'Server worker ' + cluster.worker.id;
  message += ' booted at ' + logger.timestamp();

  console.log(message);
}

function systemBooted() {
  bootedSystems++;

  if (bootedSystems === systemsToBoot.length) {
    startListening();
  }
}

// process error and output response, not to be used by the web interface,
// unless the 404 page is not found
function processError(error, res) {

  var header = sysOps.corsHeader('text/plain');

  if (debug) {
    console.log(error);
  }

  switch (error.code) {
  case 'ENOENT':
  case 'MODULE_NOT_FOUND':
    res.writeHead(404, header);
    res.write('404');

    break;

  default:
    res.writeHead(500, header);

    res.write('500\n' + error.toString());

    if (!debug) {
      console.log(error);
    }

    break;
  }

  res.end();
}

function outputStaticFile(file, res, header, code) {
  res.writeHead(code || 200, header);

  res.end(file, 'binary');
}

function getHeader(pathName) {

  var pathParts = pathName.split('.');

  var header;

  var mime;

  if (pathParts.length) {
    var extension = pathParts[pathParts.length - 1];
    mime = MIMETYPES[extension.toLowerCase()] || 'text/plain';

  } else {
    mime = 'text/plain';
  }

  return sysOps.corsHeader(mime);
}

function readFileContent(pathName, modifiedTime, res, callback) {
  fs.readFile(__dirname + FE_PATH + pathName, function(err, data) {
    var file = {
      mTime : modifiedTime,
      content : data
    };

    if (!debug) {
      filesCache[pathName] = file;
    }

    callback(file);

  });
}

// checks if the file was modified after the time the header provided and
// returns 304 if so.
function checkForFileMTime(pathName, req, file, res, code) {
  var lastModified = req ? req.headers['if-modified-since'] : null;

  if (lastModified === file.mTime.toString() && !disable304) {
    res.writeHead(304);
    res.end();
  } else {
    var header = getHeader(pathName);
    header['last-modified'] = file.mTime;

    outputStaticFile(file.content, res, header, code);
  }
}

// reads file stats to find out if theres a new version
function readFileStats(pathName, res, retry, callback) {
  fs.stat(__dirname + FE_PATH + pathName, function gotStats(error, stats) {
    if (error) {
      if (retry) {
        processError(error, res);
      } else {
        findAndOutputFile('/404.html', res);
      }
    } else {
      readFileContent(pathName, stats.mtime, res, callback);
    }
  });
}

// the starting point to output a file
function findAndOutputFile(pathName, res, req) {

  var file;

  if (!debug) {
    file = filesCache[pathName];
  }

  // retry means we tried to get a file, could not find it and now we are
  // outputting the 404 page
  // refer to readFileStats
  var retry = req ? false : true;

  if (!file) {
    readFileStats(pathName, res, retry, function gotFile(file) {
      checkForFileMTime(pathName, req, file, res, retry ? 404 : null);
    });
  } else {
    checkForFileMTime(pathName, req, file, res, retry ? 404 : null);
  }
}

// checks if the user is using a route that indicates a hub or a thread page and
// outputs the proper routed page if so
function lookingForHub(pathName, res, req) {

  // If we don't cut it, the length becomes messed ( empty element at the end)
  // and the following operations fail
  if (pathName.charAt(pathName.length - 1) === '/') {
    pathName = pathName.substr(0, pathName.length - 1);
  }

  var pathArray = pathName.split('/');

  var validHubUrl = pathArray.length > 1 && !/\W/.test(pathArray[1]);

  // we are not looking for a resource directory
  if (validHubUrl && resourceDirectories.indexOf(pathArray[1]) === -1) {

    if (pathArray.length > 2 && pathArray[2].length) {

      findAndOutputFile('/showThread.html', res, req);

    } else if (pathArray.length === 2) {
      findAndOutputFile('/showHub.html', res, req);
    } else {
      findAndOutputFile('/404.html', res);
    }

    return true;
  }

  return false;
}

function callFrontEnd(req, res) {

  var pathName = url.parse(req.url).pathname;

  var pathArray = pathName.split('/');

  // check if we are looking for a resource and trim the path so we ignore the
  // route parts
  for (var i = 0; i < pathArray.length && i < MAX_PATH_PARTS; i++) {

    var pathPart = pathArray[i];

    if (resourceDirectories.indexOf(pathPart) > -1) {
      pathArray.splice(0, i);

      pathName = '/' + pathArray.join('/');

      break;
    }

  }

  if (pathName === '/') {
    findAndOutputFile('/index.html', res, req);
  } else if (routedPages.indexOf(pathName) !== -1) {
    findAndOutputFile('/404.html', res);
  } else if (!lookingForHub(pathName, res, req)) {

    findAndOutputFile(pathName, res, req);
  }
}

function callBackEnd(req, res) {

  var pathName = url.parse(req.url).pathname;

  try {
    if (debug) {
      var module = require.resolve('.' + BE_PATH + pathName);
      delete require.cache[module];
    }

    require('.' + BE_PATH + pathName).process(req, res);

  } catch (error) {

    processError(error, res);

  }
}

// starting point for a request
function main(req, res) {

  if (!booted) {
    req.connection.destroy();
    return;
  }

  if (!req.headers || !req.headers.host) {
    res.writeHead(200, sysOps.corsHeader('text/plain'));
    res.end('get fucked, m8 :^)');
    return;
  }

  var subdomain = req.headers.host.split('.');

  if (subdomain.length && subdomain[0] === 'api') {
    callBackEnd(req, res);
  } else {
    callFrontEnd(req, res);
  }
}

function getDecayedComments(comments) {

  for (var i = 0; i < comments.length; i++) {
    var comment = comments[i];

    comment.score = sysOps.calculateScore(comment.creation,comment.commentCount);

    comment.comments = getDecayedComments(comment.comments);

  }

  return comments;

}

function updateComments(db, threadsCursor, thread) {

  var threads = db.threads();

  threads.update({
    _id : thread._id
  }, {
    $set : {
      comments : getDecayedComments(thread.comments)
    }
  }, function updatedThread(error, results) {

    if (error) {
      console.log(error);
    } else {
      iterateComments(db, threadsCursor);

    }

  });
}

function iterateComments(db, threadsCursor) {

  var threads = db.threads();

  threadsCursor.next(function(error, thread) {

    if (error) {
      console.log(error);
    } else if (thread) {
      updateComments(db, threadsCursor, thread);
    } else {
      scheduleDecay(db);
    }

  });
}

function getComments(db) {

  var threads = db.threads();

  threads.find({}, {
    comments : 1
  }, function gotCursor(error, threadsCursor) {
    if (error) {
      console.log(error);
    } else {
      iterateComments(db, threadsCursor);
    }

  });

}

/*
function decayHubs(db) {

  var hubs = db.hubs();

  hubs.find({}).forEach(function(hub){
      hubs.update({
          _id:hub._id
      },{
          $set:{
              score: sysOps.calculateScore(hub.creation,hub.commentCount+hub.threadCount) // ThreadCount counts for the hubscore too
          }
      },function updatedHubScore(error) {
          if (error) {
            console.log(error);
          }
        });
  });

}
*/

function removeThreadsToDelete(db, threadsToRemove) {
  var hubs = db.hubs();

  if (!threadsToRemove.length) {
    // All threads deleted/queue is empty -> Delete hubs with 0 threads
    hubs.remove({
      threadCount : {
        $lt : 1
      }
    }, {
      w : 1
    }, function removedEmptyHubs(error, tempResults) {
      if (error) {
        console.log(error);
      } else {
        getComments(db); // Decay comments
      }
    });

    return;

  }

  var toRemove = threadsToRemove.shift(); // Go through thread to delete queue

  var threads = db.threads();

  threads.remove({
    hubName : toRemove._id,
    threadId : {
      $in : toRemove.threads
    }
  }, {
    w : 1
  }, function removedThreads(error, results) {

    if (error) {
      console.log(error);
    } else {

      var toDecrease = results.result.n * -1;

      hubs.update({
        hubName : toRemove._id
      }, {
        $inc : {
          threadCount : toDecrease
        }
      }, function updatedThreadCount(error, result) {
        if (error) {
          console.log(error);

        } else {
          removeThreadsToDelete(db, threadsToRemove);
        }
      });

    }

  });

}

function getThreadsToDelete(db) {
  var threads = db.threads();

  threads.aggregate([ {
    $sort : {
      score : -1
    }
  }, {
    $skip : settings.minimumThreadCount
  }, {
    $match : {
      score : {
        $lt : settings.scoreTreshold
      }
    }
  }, {
    $group : {
      _id : '$hubName',
      threads : {
        $push : '$threadId'
      }
    }
  } ], function gotThreadsToDelete(error, threadsToRemove) {
    if (error) {
      console.log(error);
    } else {
      removeThreadsToDelete(db, threadsToRemove);
    }
  });

}

function decayThreads(db) {

  var threads = db.threads();

  threads.find({}).forEach(function(thread){
      threads.update({
          _id:thread._id
      },{
          $set:{
              score: sysOps.calculateScore(thread.creation,thread.commentCount)
          }
      },function updatedThreadScore(error) {
          if (error) {
            console.log(error);
          }
        });
  });

  getThreadsToDelete(db);

  /*
  // Removed.. hubs don't need a score
  threads.update({}, {
    $set : {
      score : 0
    }
  }, {
    multi : true
  }, function updatedThreads(error, results) {
    if (error) {
      console.log(error);
    } else {

      getThreadsToDelete(db);

    }
  });  */

}

function scheduleDecay(db) {
  setTimeout(function tick() {

    decayThreads(db);
  }, 1000 * 60 * settings.decayTick);
}

function startMasterThread(db) {

  try {
    var settings = JSON.parse(fs.readFileSync(__dirname + '/settings.json'));

  } catch (error) {
    console.log(error);
    console.log('Could not load settings.');
    return;
  }

  scheduleDecay(db);

  // main thread objects
  var MINIMUM_WORKER_UPTIME = 1000;
  var forkTime = {};

  for (var i = 0; i < require('os').cpus().length; i++) {
    cluster.fork();
  }

  cluster.on('fork', function(worker) {

    forkTime[worker.id] = new Date().getTime();

  });

  cluster.on('exit', function(worker, code, signal) {
    console.log('Server worker ' + worker.id + ' crashed.');

    if (new Date().getTime() - forkTime[worker.id] < MINIMUM_WORKER_UPTIME) {
      console.log('Crash on boot, not restarting it.');
    } else {
      cluster.fork();
    }

    delete forkTime[worker.id];
  });
}

if (cluster.isMaster) {

  var db = require('./db');
  db.init(function dbStarted(error) {
    if (error) {
      console.log(error);
    } else {
      startMasterThread(db);
    }
  });

} else {

  // fork objects
  var logger = require('./logger');
  var url = require('url');


  var DEFAULT_ADDRESS = '127.0.0.1';
  var DEFAULT_PORT = 8080;

  // these pages cannot be accessed directly, only by a route of a hub or thread
  var routedPages = [ 'showHub.html', 'showThread.html' ];

  var bootedSystems = 0;
  var booted = false;

  var FE_PATH = '/../fe/files';
  var BE_PATH = '/public';

  var MIMETYPES = {
    html : 'text/html',
    htm : 'text/html',
    otf : 'application/x-font-otf',
    ttf : 'application/x-font-ttf',
    woff : 'application/x-font-woff',
    js : 'application/javascript',
    css : 'text/css',
    png : 'image/png'
  };

  // these directories contain resources and the path to them must be trimmed so
  // they be used by routed pages
  var resourceDirectories = [];

  try {
    var feContent = fs.readdirSync(__dirname + FE_PATH);

    for (var i = 0; i < feContent.length; i++) {
      var file = feContent[i];

      var stats = fs.statSync(__dirname + FE_PATH + '/' + file);

      if (stats.isDirectory()) {
        resourceDirectories.push(file);
      }
    }

  } catch (error) {
    console.log(error);
    console.log('Could not read resource directories.');
    return;
  }

  sysOps.setResourceDirectories(resourceDirectories);
  sysOps.setSettings(settings);
  // so we won't hang on CPU at callFrontEnd's for
  var MAX_PATH_PARTS = 10;

  // cache to be used if debug argument is not provided so we don't hit the disk
  // for every request
  var filesCache = {};

  // if debug argument is provided, don't cache back-end modules and static
  // files in RAM
  var debug = process.argv.toString().indexOf('debug') > -1;

  var disable304 = settings.disable304;
  var address = settings.address || DEFAULT_ADDRESS;
  var port = settings.port || DEFAULT_PORT;

  var systemsToBoot = [ 'db' ];

  var systemBootCallback = function(error) {
    if (!error) {
      systemBooted();
    } else {
      console.log(error);
      console.log('Failed to boot a system');
    }
  };

  for (var i = 0; i < systemsToBoot.length; i++) {

    var system = systemsToBoot[i];

    require('./' + system).init(systemBootCallback);
  }

}
