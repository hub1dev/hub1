var sysOps = require('../sysOps');
var logger = require('../logger');
var db = require('../db');
var crypto = require('crypto');
var hubs = db.hubs();
var threads = db.threads();
var hubSpam = db.hubSpam();
var creationDelay = sysOps.getSettings().hubCreationDelay;
var expirationCache = {};


function cacheExpiration(ip, expiration) {

    var expirationMs = expiration.getTime();

    expirationCache[ip] = expirationMs;

    var now = new Date().getTime();

    var delta = expirationMs - now;

    setTimeout(function clearCache() {
        delete expirationCache[ip];
    }, delta);

    return delta;

}

function checkDbForFlood(ip, callback) {

    hubSpam.findOne({
        ip: ip,
        expiration: {
            $gte: new Date()
        }
    }, {}, function gotRegistry(error, registry) {
        if (error) {
            callback(error);
        } else if (registry) {
            callback(null, cacheExpiration(ip, registry.expiration));

        } else {
            callback();
        }
    });

}

function checkForFlood(req, callback) {

    var ip = crypto.createHash('md5').update(logger.ip(req)).digest('hex');

    var expiration = expirationCache[ip];

    var now = new Date().getTime();

    if (!expiration) {
        checkDbForFlood(ip, callback);
    } else {
        callback(null, expiration - now);
    }

}

function recordForFlood(ip, res, hubName) {

    ip = crypto.createHash('md5').update(ip).digest('hex');

    var expirationDate = logger.addMinutes(new Date(), creationDelay);

    expirationCache[ip] = expirationDate.getTime();

    cacheExpiration(ip, expirationDate);

    hubSpam.insert({
        ip: ip,
        expiration: expirationDate
    }, function insertedRecord(error, results) {
        if (error) {
            sysOps.outputError(error, res);
        } else {
            sysOps.outputResponse({
                hubName: hubName
            }, 'ok', res);
        }

    });

}

function processParameters(parameters, req, res) {

    if (sysOps.checkForBlank(parameters, [ 'hubName', 'description', 'password' ], res)) {
        return;
    }

    parameters.hubName = sysOps.validateHubName(parameters.hubName);
    if(parameters.hubName == null){
        sysOps.outputResponse(null, 'invalidHub', res);
        return;
    }

    parameters.description = parameters.description.substring(0, 256);
    parameters.password = parameters.password.substring(0, 32);

    hubs.find({hubName: parameters.hubName}).limit(1).count(function checkHubExists(error, count) {
        if (error) {
            sysOps.outputError(error, res);
        } else {
            if (count != 0) {
                // Hub exists already
                sysOps.outputResponse(null, 'hubExistsAlready', res);
                return;
            }else{
                // Create Hub
                hubs.findAndModify({
                    hubName: parameters.hubName
                }, [], {
                    $set: {
                        hubName: parameters.hubName,
                        description : parameters.description,
                        commentCount: 0,
                        creation : new Date(),
                        password : crypto.createHash('md5').update(parameters.password).digest('hex'),
                        sidebar : ''
                    },
                    $inc: {
                        threadCount: 1,
                        lastThreadId: 1
                    }
                }, {
                    upsert : true,
                    'new': true
                }, function updatedHub(error, hub) {
                    if (error) {
                        sysOps.outputError(error, res);
                    }else{
                        // Insert First Thread
                        var thread = {
                            hubName: parameters.hubName,
                            salt: 0,
                            creation: new Date(),
                            signature: null,
                            comments: [],
                            lastCommentId: 0,
                            threadId: 1,
                            commentCount: 0,
                            score: 0,
                            title: 'Welcome to '+parameters.hubName,
                            poster: 'hub1',
                            text: 'Welcome to '+parameters.hubName,
                            lastComment: new Date(),
                            types: []
                        };
                        threads.insert(thread, function createdThread(error) {
                            if (error) {
                                sysOps.outputError(error);
                            }else{
                                // Record for flood and respond with OK
                                recordForFlood(logger.ip(req), res, parameters.hubName);
                                return;
                            }
                        });
                    }
                });
            }
        }
    });

}


exports.process = function (req, res) {

    checkForFlood(req, function checkedForFloor(error, remaining) {

        if (error) {
            sysOps.outputError(error, res);
        } else if (remaining) {

            sysOps.outputResponse(remaining, 'flood', res);

        } else {

            sysOps.getParameters(req, res, function gotParameters(parameters) {

                processParameters(parameters, req, res);

            });

        }
    });

};