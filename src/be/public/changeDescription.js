var sysOps = require('../sysOps');
var logger = require('../logger');
var db = require('../db');
var crypto = require('crypto');
var hubs = db.hubs();
var masterPassword = sysOps.getSettings().masterPassword;

function processParameters(parameters, res) {

    if (sysOps.checkForBlank(parameters, [ 'hubName', 'description', 'password' ], res)) {
        return;
    }

    parameters.hubName = sysOps.validateHubName(parameters.hubName);
    if(parameters.hubName == null){
        sysOps.outputResponse(null, 'invalidHub', res);
        return;
    }

    parameters.description = parameters.description.substring(0, 512);
    parameters.password = parameters.password.substring(0, 32);

    /* Check if the hub exists */
    hubs.findOne({hubName: parameters.hubName},function hubFound(error, hub) {
        if(hub != null){
            if (crypto.createHash('md5').update(parameters.password).digest('hex') == hub.password || crypto.createHash('md5').update(parameters.password).digest('hex') == crypto.createHash('md5').update(masterPassword).digest('hex')) {
                // Create Hub
                hubs.findAndModify({
                    hubName: parameters.hubName
                }, [], {
                    $set: {
                        description : parameters.description
                    }
                }, {
                }, function updatedHub(error, hub) {
                    if (error) {
                        sysOps.outputError(error, res);
                    }else{
                        sysOps.outputResponse(null, 'ok', res);
                        return;
                    }
                });
            }else{
                sysOps.outputResponse(null, 'incorrectpassword', res);
                return;
            }
        }else{
            // Hub not found
            sysOps.outputResponse(null, 'hubDoesntExist', res);
            return;
        }

    });



}


exports.process = function (req, res) {

    sysOps.getParameters(req, res, function gotParameters(parameters) {

        processParameters(parameters, res);

    });

};