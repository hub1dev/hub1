var sysOps = require('../sysOps');
var db = require('../db');
var threads = db.threads();
var hubs = db.hubs();

/*
Parameters: hubName, skip, limit, getdescription, getsidebar
 */
function getThreads(parameters, res) {

    if (sysOps.checkForBlank(parameters, ['hubName', 'skip', 'limit'], res)) {
        return;
    }

    parameters.hubName = sysOps.validateHubName(parameters.hubName);
    if(parameters.hubName == null){
        sysOps.outputResponse(null, 'invalidHub', res);
        return;
    }

    if(isNaN(parameters.skip) || isNaN(parameters.limit)){
        sysOps.outputResponse(null, 'NaN', res);
        return;
    }

    /* Get the hub, send description and threadlist */
    hubs.findOne({hubName: parameters.hubName}, function hubFound(error, hub) {
        if (error) {
            sysOps.outputError(error, res);
        } else {
            if (hub != null) {
                threads.aggregate([{
                    $match: {
                        hubName: parameters.hubName
                    }
                }, {
                    $sort: {
                        score: -1
                    }
                },{
                    $skip: parameters.skip
                },{
                    $limit: parameters.limit
                }, {
                    $project: {
                        _id: 0,
                        threadId: 1,
                        poster: 1,
                        types: 1,
                        title: 1,
                        signature: 1,
                        lastComment: 1,
                        commentCount: 1,
                        hubName :1
                    }
                }], function gotThreads(error, threads) {
                    if (error) {
                        sysOps.outputError(error, res);
                    } else {
                        var description = undefined;
                        var sidebar = undefined;
                        if(parameters.getdescription == 'true'){
                            description = hub.description
                        }
                        if(parameters.getsidebar == 'true'){
                            sidebar = hub.sidebar
                        }
                        var result = {
                            description: description,
                            sidebar : sidebar,
                            threads: threads
                        };
                        sysOps.outputResponse(result, 'ok', res);
                    }
                });
            } else {
                // Hub not found
                sysOps.outputResponse(null, 'hubDoesntExist', res);
                return;
            }

        }

    });

}

exports.process = function (req, res) {

    sysOps.getParameters(req, res, function gotParameters(parameters) {

        getThreads(parameters, res);

    });

};
