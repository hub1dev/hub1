var sysOps = require('../sysOps');
var crypto = require('crypto');
var db = require('../db');
var threads = db.threads();
var hubs = db.hubs();
var logger = require('../logger');
var commentSpam = db.commentSpam();
var creationDelay = sysOps.getSettings().commentCreationDelay;

var expirationCache = {};

function cacheExpiration(ip, expiration) {

    var expirationMs = expiration.getTime();

    expirationCache[ip] = expirationMs;

    var now = new Date().getTime();

    var delta = expirationMs - now;

    setTimeout(function clearCache() {
        delete expirationCache[ip];
    }, delta);

    return delta;

}

function checkDbForFlood(ip, callback) {

    commentSpam.findOne({
        ip: ip,
        expiration: {
            $gte: new Date()
        }
    }, {}, function gotRegistry(error, registry) {
        if (error) {
            callback(error);
        } else if (registry) {
            callback(null, cacheExpiration(ip, registry.expiration));

        } else {
            callback();
        }
    });

}

function checkForFlood(req, callback) {

    var ip = crypto.createHash('md5').update(logger.ip(req)).digest('hex');

    var expiration = expirationCache[ip];

    var now = new Date().getTime();

    if (!expiration) {
        checkDbForFlood(ip, callback);
    } else {

        callback(null, expiration - now);
    }

}

function recordForFlood(ip, res) {

    ip = crypto.createHash('md5').update(ip).digest('hex');

    var expirationDate = logger.addMinutes(new Date(), creationDelay);

    expirationCache[ip] = expirationDate.getTime();

    cacheExpiration(ip, expirationDate);

    commentSpam.insert({
        ip: ip,
        expiration: expirationDate
    }, function insertedRecord(error, results) {
        if (error) {
            sysOps.outputError(error, res);
        } else {
            sysOps.outputResponse(null, 'ok', res);
        }

    });

}

var sortFunction = function (a, b) {

    return b.score - a.score;

};

function sortComments(comments) {

    for (var i = 0; i < comments.length; i++) {

        var comment = comments[i];

        comment.comments = sortComments(comment.comments);

    }

    return comments.sort(sortFunction);

}

function getSubcomment(tempComments, requiredId) {
    for (var j = 0; j < tempComments.length; j++) {

        var tempComment = tempComments[j];

        if (+requiredId === tempComment.commentId) {
            return tempComment;

        }

    }

    return false;
}

function adjustComments(currentComments, subComments, comment) {

    var tempComments = currentComments;

    for (var i = 0; i < subComments.length; i++) {

        var subComment = getSubcomment(tempComments, subComments[i]);

        if (!subComment) {
            return false;
        }

        subComment.commentCount = subComment.commentCount + 1;

        tempComments = subComment.comments;

    }

    tempComments.push(comment);

    return true;
}

function getUpdateBlock(hubName, threadCreation, timestamp, subComments,
                        currentComments, comment) {

    hubs.update({
        hubName: hubName
    }, {
        $inc: {
            commentCount: 1
        }
    }, function adjustedHubScore(error) {
        if (error) {
            console.log(error);
        }
    });

    if (!adjustComments(currentComments, subComments, comment)) {
        return false;
    }

    var updateBlock = {
        $inc: {
            commentCount: 1
        },
        $set: {
            lastCommentId: comment.commentId,
            lastComment: timestamp,
            comments: sortComments(currentComments)
        }
    };

    return updateBlock;

}

function getInformedPoster(parameters) {

    return parameters.poster && parameters.poster.toString().trim().length;

}

function getPoster(parameters) {

    var informedPoster = getInformedPoster(parameters);

    return informedPoster ? parameters.poster.toString().trim().substring(0, 32)
        : null;

}

function processParameters(parameters, thread, req, res) {

    parameters.text = parameters.text.toString().trim().substring(0, 8192);

    parameters.poster = getPoster(parameters);

    var ip = logger.ip(req);

    var signature = parameters.sign ? sysOps.createSignature(parameters.sign.substring(0,16), thread.salt)
        : null;

    var timestamp = new Date();

    var comment = {
        commentId: thread.lastCommentId + 1,
        poster: parameters.poster,
        text: parameters.text,
        signature: signature,
        comments: [],
        commentCount: 0,
        creation: timestamp,
        score: 0
    };

    var updateBlock = getUpdateBlock(parameters.hubName, thread.creation,
        timestamp, parameters.subComments || [], thread.comments, comment);

    if (!thread.commentCount) {
        updateBlock.$set.firstComment = timestamp;
    }

    // it means we were informed of a comment that does not exist
    if (!updateBlock) {

        sysOps.outputResponse(null, 'commentNotFound', res);

        return;
    }

    threads.update({
        hubName: parameters.hubName,
        threadId: +parameters.threadId
    }, updateBlock, function createdComment(error, result) {
        if (error) {
            sysOps.outputError(error, res);
        } else {
            recordForFlood(ip, res);
        }
    });

}

function getThread(parameters, req, res) {

    var mandatoryParameters = ['hubName', 'threadId', 'text'];
    if (sysOps.checkForBlank(parameters, mandatoryParameters, res)) {
        return;
    }

    parameters.hubName = sysOps.validateHubName(parameters.hubName);
    if(parameters.hubName == null){
        sysOps.outputResponse(null, 'invalidHub', res);
        return;
    }

    threads.findOne({
        hubName: parameters.hubName,
        threadId: +parameters.threadId
    }, {
        _id: 0,
        lastCommentId: 1,
        commentCount: 1,
        comments: 1,
        creation: 1,
        salt: 1
    }, function gotThread(error, thread) {
        if (error) {
            sysOps.outputError(error, res);
        } else if (!thread) {
            sysOps.outputResponse(null, 'threadNotFound', res);
        } else {
            processParameters(parameters, thread, req, res);
        }
    });

}

exports.process = function (req, res) {

    checkForFlood(req, function checkedForFloor(error, remaining) {

        if (error) {
            sysOps.outputError(error, res);
        } else if (remaining) {

            sysOps.outputResponse(remaining, 'flood', res);

        } else {

            sysOps.getParameters(req, res, function gotParameters(parameters) {

                getThread(parameters, req, res);

            });

        }
    });

};