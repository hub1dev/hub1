var sysOps = require('../sysOps');
var logger = require('../logger');
var db = require('../db');
var crypto = require('crypto');
var hubs = db.hubs();
var threads = db.threads();
var threadSpam = db.threadSpam();
var creationDelay = sysOps.getSettings().threadCreationDelay;
var expirationCache = {};

function cacheExpiration(ip, expiration) {

    var expirationMs = expiration.getTime();

    expirationCache[ip] = expirationMs;

    var now = new Date().getTime();

    var delta = expirationMs - now;

    setTimeout(function clearCache() {
        delete expirationCache[ip];
    }, delta);

    return delta;

}

function checkDbForFlood(ip, callback) {

    threadSpam.findOne({
        ip: ip,
        expiration: {
            $gte: new Date()
        }
    }, {}, function gotRegistry(error, registry) {
        if (error) {
            callback(error);
        } else if (registry) {
            callback(null, cacheExpiration(ip, registry.expiration));

        } else {
            callback();
        }
    });

}

function checkForFlood(req, callback) {

    var ip = crypto.createHash('md5').update(logger.ip(req)).digest('hex');

    var expiration = expirationCache[ip];

    var now = new Date().getTime();

    if (!expiration) {
        checkDbForFlood(ip, callback);
    } else {
        callback(null, expiration - now);
    }

}

function recordForFlood(ip, res, threadId) {

    ip = crypto.createHash('md5').update(ip).digest('hex');

    var expirationDate = logger.addMinutes(new Date(), creationDelay);

    expirationCache[ip] = expirationDate.getTime();

    cacheExpiration(ip, expirationDate);

    threadSpam.insert({
        ip: ip,
        expiration: expirationDate
    }, function insertedRecord(error, results) {
        if (error) {
            sysOps.outputError(error, res);
        } else {
            sysOps.outputResponse({
                threadId: threadId
            }, 'ok', res);
        }

    });

}

function processError(hub, originalError, res) {
    hubs.update({
        hubName: hub
    }, {
        $inc: {
            threadCount: -1,
            lastThreadId: -1
        }
    }, function adjustedThreadCount(error) {

        sysOps.outputError(error || originalError, res);

    });

}

function getTypes(desiredTypes) {

    var actualTypes = [];

    for (var i = 0; i < desiredTypes.length; i++) {

        var type = desiredTypes[i].toString().trim().substring(0, 8);

        if (type.length && actualTypes.indexOf(type) === -1) {
            actualTypes.push(type);
        }

    }

    return actualTypes;

}

function getInformedPoster(parameters) {
    return parameters.poster && parameters.poster.toString().trim().length;

}

function createThread(threadId, parameters, req, res) {

    var timestamp = new Date();

    var informedPoster = getInformedPoster(parameters);

    var poster = informedPoster ? parameters.poster.toString().trim().substring(
        0, 32) : null;

    var title = parameters.title.toString().trim().substring(0, 256);

    var text = parameters.text.toString().trim().substring(0, 8192);

    var ip = logger.ip(req);

    var saltInput = timestamp + parameters.hubName;
    saltInput += ip + Math.random();

    var salt = crypto.createHash('sha256').update(saltInput).digest('hex')
        .substring(0, 8);

    var signature = parameters.sign ? sysOps.createSignature(parameters.sign.substring(0,16), salt) : null;

    var thread = {
        hubName: parameters.hubName,
        salt: salt,
        creation: timestamp,
        signature: signature,
        comments: [],
        lastCommentId: 0,
        threadId: threadId,
        commentCount: 0,
        score: 0,
        title: title,
        poster: poster,
        text: text,
        lastComment: timestamp,
        types: getTypes(parameters.types)
    };

    threads.insert(thread, function createdThread(error) {
        if (error) {
            processError(parameters.hubName, error);
        } else {

            recordForFlood(ip, res, threadId);

        }

    });

}

function processParameters(parameters, req, res) {

    if (sysOps.checkForBlank(parameters, ['hubName', 'title', 'text'], res)) {
        return;
    }


    parameters.hubName = sysOps.validateHubName(parameters.hubName);
    if(parameters.hubName == null){
        sysOps.outputResponse(null, 'invalidHub', res);
        return;
    }

    // check if hub exists and update threadcloud and lastthreadid
    hubs.find({hubName: parameters.hubName}).limit(1).count(function hubExists(error, count) {
        if (error) {
            sysOps.outputError(error, res);
        } else {
            if (count != 0) {
                // Hub exists, update it and create the thread
                hubs.findAndModify({
                    hubName: parameters.hubName
                }, [], {
                    $inc: {
                        threadCount: 1,
                        lastThreadId: 1
                    }
                }, {
                    'new': true
                }, function updatedHub(error, hub) {
                    if (error) {
                        sysOps.outputError(error, res);
                    } else {
                        createThread(hub.value.lastThreadId, parameters, req, res);
                    }
                });
            } else {
                // hub does not exist
                sysOps.outputResponse(null, 'hubDoesntExist', res);
                return;
            }
        }
    });
}

exports.process = function (req, res) {

    checkForFlood(req, function checkedForFloor(error, remaining) {

        if (error) {
            sysOps.outputError(error, res);
        } else if (remaining) {

            sysOps.outputResponse(remaining, 'flood', res);

        } else {

            sysOps.getParameters(req, res, function gotParameters(parameters) {

                processParameters(parameters, req, res);

            });

        }
    });

};