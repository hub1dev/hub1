var sysOps = require('../sysOps');
var threads = require('../db').threads();

function getThread(parameters, res) {

  if (sysOps.checkForBlank(parameters, [ 'hubName', 'threadId', 'getComments' ], res)) {
    return;
  }

  parameters.hubName = sysOps.validateHubName(parameters.hubName);
  if(parameters.hubName == null){
    sysOps.outputResponse(null, 'invalidHub', res);
    return;
  }

  var getfields = {
      _id : 0,
      text : 1,
      title : 1,
      types : 1,
      firstComment : 1,
      lastComment : 1,
      comments : 1,
      commentCount : 1,
      poster : 1,
      signature : 1,
      creation : 1
  }
  if(parameters.getComments == 'false'){
      // All usual fields without the comments
      var getfields = {
          _id : 0,
          text : 1,
          title : 1,
          types : 1,
          firstComment : 1,
          lastComment : 1,
          commentCount : 1,
          poster : 1,
          signature : 1,
          creation : 1
      }
  }

  threads.findOne({
    hubName : parameters.hubName.toString().toString().trim().toLowerCase(),
    threadId : +parameters.threadId
  }, getfields, function gotThread(error, thread) {
    if (error) {
      sysOps.outputError(error, res);
    } else if (!thread) {
      sysOps.outputResponse(null, 'threadNotFound', res);
    } else {
      sysOps.outputResponse(thread, 'ok', res);
    }
  });



}

exports.process = function(req, res) {

  sysOps.getParameters(req, res, function gotParameters(parameters) {

    getThread(parameters, res);

  });

};