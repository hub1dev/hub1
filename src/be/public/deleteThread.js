var sysOps = require('../sysOps');
var logger = require('../logger');
var db = require('../db');
var crypto = require('crypto');
var hubs = db.hubs();
var threads = db.threads();
var masterPassword = sysOps.getSettings().masterPassword;

function processParameters(parameters, res) {

    if (sysOps.checkForBlank(parameters, [ 'hubName', 'threadId', 'password' ], res)) {
        return;
    }

    parameters.hubName = sysOps.validateHubName(parameters.hubName);
    if(parameters.hubName == null){
        sysOps.outputResponse(null, 'invalidHub', res);
        return;
    }

    parameters.password = parameters.password.substring(0, 32);

    hubs.findOne({hubName: parameters.hubName}, function hubFound(error, hub) {
        if (error) {
            sysOps.outputError(error, res);
        } else {
            if (hub != null) {
                if (crypto.createHash('md5').update(parameters.password).digest('hex') == hub.password || crypto.createHash('md5').update(parameters.password).digest('hex') == crypto.createHash('md5').update(masterPassword).digest('hex')) {
                    threads.removeOne({
                        hubName: parameters.hubName,
                        threadId: parseInt(parameters.threadId)
                    }, { }, function threadRemoved(error, removedThread) {
                        if (error) {
                            sysOps.outputError(error);
                        } else {
                            if (removedThread.result.n != 0) {
                                hubs.update({
                                    hubName: parameters.hubName
                                }, {
                                    $inc: {
                                        threadCount: -1
                                    }
                                }, function updatedThreadCount(error, result) {
                                    if (error) {
                                        sysOps.outputError(error);
                                    }else{
                                        sysOps.outputResponse(null, 'ok', res);
                                        return;
                                    }
                                });
                            } else {
                                sysOps.outputResponse(null, 'threadNotFound', res);
                                return;
                            }
                        }
                    });
                } else {
                    sysOps.outputResponse(null, 'incorrectpassword', res);
                    return;
                }
            } else {
                // Hub not found
                sysOps.outputResponse(null, 'hubDoesntExist', res);
                return;
            }
        }
    });
}


exports.process = function (req, res) {

    sysOps.getParameters(req, res, function gotParameters(parameters) {

        processParameters(parameters, res);

    });

};