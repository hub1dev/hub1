var sysOps = require('../sysOps');
var threads = require('../db').threads();
var threadsToReturn = sysOps.getSettings().topThreadsCount;

function getThreads(res) {

  threads.aggregate([ {
    $sort : {
      score : -1
    }
  }, {
    $limit : threadsToReturn
  }, {
    $project : {
      _id : 0,
      threadId : 1,
      hubName : 1,
      poster : 1,
      types : 1,
      title : 1,
      signature : 1,
      lastComment : 1,
      commentCount : 1
    }
  } ], function gotThreads(error, threads) {
    if (error) {
      sysOps.outputError(error, res);
    } else {
      sysOps.outputResponse(threads, 'ok', res);
    }
  });

}

exports.process = function(req, res) {

  sysOps.getParameters(req, res, function gotParameters(parameters) {

    getThreads(res);

  });

};
