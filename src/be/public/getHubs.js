var sysOps = require('../sysOps');
var hubs = require('../db').hubs();

function getHubs(res) {

  hubs.aggregate([ {
    $sort : {
        threadCount : -1
    }
  }, {
    $project : {
      _id : 0,
      hubName : 1,
      threadCount : 1,
      description : 1
    }
  }/*, {
   $limit : 50
   }*/], function gotHubs(error, hubs) {
    if (error) {
      sysOps.outputError(error, res);
    } else {
      sysOps.outputResponse(hubs, 'ok', res);
    }
  });

}

exports.process = function(req, res) {

  sysOps.getAnonJsonBody(req, res, function interfaceMatches() {
    getHubs(res);
  });

};