var crypto = require('crypto');
var logger = require('./logger');

var REQUEST_LIMIT_SIZE = 1e6;// 1 million characters if I'm not mistaken

var version = '0.0.2';
var settings;
var resourceDirectories;

exports.setSettings = function(newSettings) {
  settings = newSettings;
};

exports.getSettings = function() {
  return settings;
};

exports.setResourceDirectories = function(newDirectories) {
  resourceDirectories = newDirectories;
};

exports.resourceDirectories = function() {
  return resourceDirectories;
};

// see how many hours have passed since creation, divide by it. if less than one
// hour has passed, divide by one
exports.adjustScore = function(score, creation) {

  var match = creation.match(/^(\d+)-(\d+)-(\d+)_(\d+)\:(\d+)\:(\d+)$/);
  var date = Date.UTC(match[1], match[2] - 1, match[3], match[4], match[5],
      match[6]);

  var now = new Date().getTime();

  var differenceInHours = (now - date) / 1000 / 60 / 60;

  return score / (differenceInHours < 1 ? 1 : differenceInHours);

};

exports.createSignature = function(ip, salt) {

  return crypto.createHash('sha256').update(ip + salt).digest('hex').substring(
      0, 8);

};

// It uses the provided contentType and builds a header ready for CORS.
// Currently it just allows everything.
exports.corsHeader = function(contentType) {
  return {
    'Content-Type' : contentType,
    'access-control-allow-origin' : '*'
  };
};

exports.getParameters = function(req, res, callback) {

  exports.getAnonJsonBody(req, res, function gotBody(body) {
    callback(body.parameters);
  });

};

// Sends back the jsonobject contained in the request to the callback. Will
// output error if can't get anything.
exports.getAnonJsonBody = function(req, res, callback) {

  var body = '';

  req.on('data', function dataReceived(data) {
    body += data;

    if (body.length > REQUEST_LIMIT_SIZE) {
      exports.outputResponse(null, 'tooLarge', res);

      req.connection.destroy();
    }
  });

  req.on('end', function dataEnded() {
    var parsedData;

    if (settings.verbose) {
      console.log('\n\ninput: ' + body);
    }

    try {
      parsedData = JSON.parse(body);

    } catch (error) {
      if (settings.verbose) {
        console.log(error);
      }

      exports.outputResponse(null, 'parseError', res);

    }

    if (!parsedData) {
      return;
    } else if (parsedData.interfaceVersion === version) {
      callback(parsedData);
    } else {
      exports.outputResponse(version, 'interfaceMismatch', res);
    }

  });
};

// Shortcut to output a response with the provided blocks and status.
exports.outputResponse = function(data, status, res) {
  if (!res) {
    console.log('null res ' + status);
    return;
  }

  var output = {
    status : status,
    data : data || null
  };

  res.writeHead(200, exports.corsHeader('application/json'));

  if (settings.verbose) {
    console.log('\noutput: ' + JSON.stringify(output));
  }

  res.end(JSON.stringify(output));
};

// Shortcut to just print and output error
exports.outputError = function(error, res) {

  console.log(error);

  exports.outputResponse(null, 'error', res);
};

// Calculcate the score (reddit hotranking)
exports.calculateScore = function(creationTime, commentCount){
    return Math.log10(Math.max(commentCount,1))+((creationTime.getTime()-new Date().getTime())/1000)/45000;
}

exports.validateHubName = function(hubName){
  /* Hubname check */
  hubName = hubName.toString().trim().substring(0, 64).toLowerCase();
  if (/\W/.test(hubName)) {
    return null;
  } else if (resourceDirectories.indexOf(hubName) > -1) {
    return null;
  }
  return hubName;
}



// Shortcut to check if the object contains all the parameters and they are not
// empty. Will not terminate the connection if the response object is not
// passed.
exports.checkForBlank = function(object, parameters, res) {
  function failCheck(parameter, reason) {

    if (settings.verbose) {
      console.log('Blank reason: ' + reason);
    }

    if (res) {
      exports.outputResponse(parameter, 'blank', res);
    }

    return true;
  }

  if (!object) {

    failCheck();

    return true;

  }

  for (var i = 0; i < parameters.length; i++) {
    var parameter = parameters[i];

    if (!object.hasOwnProperty(parameter)) {
      return failCheck(parameter, 'no parameter');

    }

    if (object[parameter] === null) {
      return failCheck(parameter, 'null');
    }

    if (object[parameter] === undefined) {
      return failCheck(parameter, 'undefined');
    }

    if (!object[parameter].toString().trim().length) {
      return failCheck(parameter, 'length');
    }
  }

  return false;
};
