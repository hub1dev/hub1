var current_hub = "";
var current_hub_sidebar = "";


var moderatormode;  // If moderatormode is on, edit and delete buttons are generated
var moderatormode_password; // Temporary password save

var loading_threads_status = 0; // 0 = ready, 1 = loading, 2 = all threads loaded
var last_loaded_thread = 0;

$(document).ready(function () {

    $("#button_cancel").click(function () {
        $("#div_newthread").hide();
        return false;
    });

    $("#button_createthread").click(function () {
        // retrive username and keyphrase from localstorage
        if(localStorage.username != ""){
            $("#input_poster").val(localStorage.username)
        }else{
            $("#input_poster").val('');
        }
        if(localStorage.keyphrase != "") {
            $('#div_keyphrase').show();
            $('#input_identify').prop('checked',true);
            $('#input_keyphrase').val(localStorage.keyphrase);
        }else{
            $('#input_identify').prop('checked',false);
            $('#input_keyphrase').val('');
        }

        // Empty inputfield
        var editors = $('.CodeMirror');
        for(var x=0; x < editors.length; x++){
            editors[x].CodeMirror.setValue('');
        }

        // Show must be executed first, or the editors preview box is too small
        $("#div_newthread").show();
        // Init Markdwon Editor
        $.UIkit.htmleditor($('#input_text'));
        return false;
    });

    $('#input_identify').click(function () {
        if ($("#input_identify")[0].checked) {
            $('#div_keyphrase').show();
        } else {
            $('#div_keyphrase').hide();
        }
    });

    $("#button_submit").click(function () {
        var sign = null;
        if ($('#input_identify')[0].checked) {
            sign = $('#input_keyphrase').val();
        }
        create_thread($("#input_title").val(), $("#input_text").val(), sign, $("#input_poster").val());
        return false;
    });

    $('#button_hub_submit').click(function () {
        if ($('#input_hub_password').val() == $('#input_hub_password2').val()) {
            create_hub(current_hub, $("#input_hub_description").val(), $("#input_hub_password").val());
        } else {
            UIkit.notify({
                message: '<i class="uk-icon-warning"></i> Passwords don\'t match',
                status: 'info',
                timeout: 5000,
                pos: 'top-center'
            });
        }

        return false;
    });

    // show Password box
    $('#button_moderate').click(function () {
        $('#div_password').show();
        $('#button_moderate').hide();
        return false;
    });

    // Validate Password
    $('#input_password').keypress(function (e) {
        if (e.which == 13) {
            checkModerator(current_hub, $('#input_password').val());
            return false;
        }
    });

    // Moderator login
    $('#button_password_submit').click(function () {
        checkModerator(current_hub, $('#input_password').val());
    });

    // Preview
    $(document).on('click', "[id^=button-preview-]", function () {
        var thread_id = $(this).attr('id').split('-');
        var request = {
            interfaceVersion: settings.interfaceversion,
            parameters: {
                hubName: thread_id[2],
                threadId: thread_id[3],
                getComments: 'false'
            }
        }
        var request = JSON.stringify(request);
        $('#div-preview-thread-'+ thread_id[2]+'-'+ thread_id[3]).show();
        $.ajax({
            type: 'POST',
            url: 'http://api.' + settings.apidomain + '/threadDetails',
            data: request,
            dataType: 'JSON'
        }).done(function (data) {
            if (data['status'] == 'ok') {
                $('#div-preview-thread-'+ thread_id[2]+'-'+ thread_id[3]).html(marked(data['data'].text));
                $('#button-preview-'+ thread_id[2]+'-'+ thread_id[3]).html('<i class="uk-icon-check"/>');
            }
        });
    });

    // Delete thread command
    $(document).on('click', "[id^=delete_thread_]", function () {
        if ($(this).html() != 'sure?') {
            $(this).html('sure?');
        } else if ($(this).html() == 'sure?') {
            var thread_id = $(this).attr('id').split('_');
            delete_thread(current_hub, thread_id[2], moderatormode_password);
        }
    });

    // Click sidebar
    $('#button_sidebar').click(function () {
        // Set sidebar
        if(current_hub_sidebar != undefined && current_hub_sidebar != null){
            $('#div_sidebar_text').html(marked(current_hub_sidebar));
        }
    });
    // Change sidebar
    $('#button_change_sidebar').click(function () {
        $('#input_change_sidebar').val(current_hub_sidebar);
        $.UIkit.htmleditor($('#input_change_sidebar'));
    });

    // Submit change sidebar button
    $('#button_change_sidebar_submit').click(function () {
        change_sidebar(current_hub, moderatormode_password, $('#input_change_sidebar').val());
    });

    // Change descripption
    $('#button_change_description').click(function () {
        $('#input_change_description').val($('#a_hub_description').text());
    });

    // Submit change description enter
    $('#input_change_description').keypress(function (e) {
        if (e.which == 13) {
            change_description(current_hub, moderatormode_password, $('#input_change_description').val());
            return false;
        }
    });

    // Submit change description button
    $('#button_change_description_submit').click(function () {
        change_description(current_hub, moderatormode_password, $('#input_change_description').val());
    });



    // Load on scroll
    $(window).scroll(function() {
       if($(window).scrollTop() + $(window).height() > $(document).height() - 100 && loading_threads_status == 0) {
            load_hub(false);
       }
    });


    // Init
    init();
    load_hub(true);
});

function init() {
    // Get current hub

    if (settings.test_showhub_url != null) {
        var url = settings.test_showhub_url;
    } else {
        var url = window.location.href;
    }
    url = url.replace('http://', '').replace('https://', '');

    var urlparts = url.split('/');
    if (urlparts.length == 2) {
        current_hub = urlparts[1];
    }

    // Set logo header link
    $("#link_header").attr('href', 'http://www.' + settings.domain);
    // Set Title
    $("#title_title").text(current_hub);
    // Set Breadcrumbs
    $("#a_breadcrumb_1").attr('href', 'http://www.' + settings.domain);
    $("#a_breadcrumb_1").text('hub1');
    $("#a_breadcrumb_2").text(current_hub);
}

function change_sidebar(hub, password, newsidebar) {
    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters: {
            hubName: hub,
            password: password,
            sidebar: newsidebar
        }
    }

    var request = JSON.stringify(request);

    $.ajax({
        type: 'POST',
        url: 'http://api.' + settings.apidomain + '/changeSidebar',
        data: request,
        dataType: 'JSON'
    }).done(function (data) {

        if (data['status'] == 'ok') {
            message = '<i class="uk-icon-check"></i> Sidebar changed';
            load_hub(true);
        } else if (data['status'] == 'incorrectpassword') {
            message = '<i class="uk-icon-warning"></i> Password incorrect';
        } else {
            message = '<i class="uk-icon-warning"></i> Error: ' + data['status'];
        }

        UIkit.notify({
            message: message,
            status: 'info',
            timeout: 5000,
            pos: 'top-center'
        });
    });
}

function change_description(hub, password, newdescription) {
    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters: {
            hubName: hub,
            password: password,
            description: newdescription
        }
    }

    var request = JSON.stringify(request);

    $.ajax({
        type: 'POST',
        url: 'http://api.' + settings.apidomain + '/changeDescription',
        data: request,
        dataType: 'JSON'
    }).done(function (data) {

        if (data['status'] == 'ok') {
            message = '<i class="uk-icon-check"></i> Description changed';
            load_hub(true);
        } else if (data['status'] == 'incorrectpassword') {
            message = '<i class="uk-icon-warning"></i> Password incorrect';
        } else if (data['status'] == 'blank') {
            message = '<i class="uk-icon-warning"></i> Description can´t be  empty';
        } else {
            message = '<i class="uk-icon-warning"></i> Error: ' + data['status'];
        }

        UIkit.notify({
            message: message,
            status: 'info',
            timeout: 5000,
            pos: 'top-center'
        });
    });
}

function checkModerator(hub, password) {
    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters: {
            hubName: hub,
            password: password
        }
    }

    var request = JSON.stringify(request);

    $.ajax({
        type: 'POST',
        url: 'http://api.' + settings.apidomain + '/checkModerator',
        data: request,
        dataType: 'JSON'
    }).done(function (data) {
        if (data['status'] == 'ok') {
            moderatormode = true;
            moderatormode_password = password;
            $('#div_password').hide();
            $('#button_moderate').hide();
            load_hub(true);
            message = '<i class="uk-icon-check"></i> You are now a moderator';
        } else if (data['status'] == 'incorrectpassword') {
            $('#div_password').hide();
            $('#button_moderate').show();
            message = '<i class="uk-icon-warning"></i> Password incorrect';
        } else {
            $('#div_password').hide();
            $('#button_moderate').show();
            message = '<i class="uk-icon-warning"></i> Error: ' + data['status'];
        }

        $('#input_password').val('');

        UIkit.notify({
            message: message,
            status: 'info',
            timeout: 5000,
            pos: 'top-center'
        });
    });
}


// Loads the next 10 threads
function load_hub(reset) {
    if(reset == true){
        $('#table_threads').html('');
        last_loaded_thread = 0;
        loading_threads_status = 0;
    }

    // Load just enough threads to fill the screen but at least 10
    var threads_per_request = Math.ceil($( window ).height()/40);
    if(threads_per_request < 10){
        threads_per_request = 10;
    }

    // Check if this is a reset and description and sidebar information should be received
    var getdescription = 'false';
    var getsidebar = 'false';

    if(reset){
        var getdescription = 'true';
        var getsidebar = 'true';
    }

    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters: {
            hubName: current_hub,
            skip: last_loaded_thread,
            limit: threads_per_request,
            getdescription : getdescription,
            getsidebar : getsidebar
        }
    }

    var request = JSON.stringify(request);

    // Load Threads
    $("#img_ajax_loader").show();
    loading_threads_status = 1; // Loading

    $.ajax({
        type: 'POST',
        url: 'http://api.' + settings.apidomain + '/hubThreads',
        data: request,
        dataType: 'JSON'
    }).done(function (data) {
        if (data['status'] == 'ok') {
            if(data['data']['threads'].length == 0){
                loading_threads_status = 2; // Done
            }else{
                if(reset){
                    // Set hub title
                    $('#a_hub_title').text(current_hub);

                    // Set description if it is a reset
                    $('#a_hub_description').text(data['data']['description']);
                    current_hub_sidebar = data['data']['sidebar'];
                }

                last_loaded_thread = last_loaded_thread + data['data']['threads'].length;
                loading_threads_status = 0; // More threads to load
            }
            add_threads(data['data']['threads']);
        } else if (data['status'] == 'hubDoesntExist') {
            $('#a_hub_title').text(current_hub);
            $('#navbar').hide();
            $('#div_newhub').show();
        } else {
            $('#table_threads').html('<tr><td> Error:' + data['status'] + '</td></tr>');
        }

        $("#img_ajax_loader").hide();
    });
}

function add_threads(threads){
    var htmlout = getHTML_threadList(threads, false, moderatormode);
    if (htmlout != null) {
        $('#table_threads').html($('#table_threads').html()+htmlout);
    }

    if (moderatormode) {
        $('#button_change_description').show();
        $('#button_change_sidebar').show();
    }else{
        $('#button_change_description').hide();
        $('#button_change_sidebar').hide();
    }

    // Set usernames with html escape
    for (var index in threads) {
        $('#hub_link_'+threads[index].hubName+'_' + threads[index].threadId).text(threads[index].title);
        var poster = threads[index].poster;
        if (poster == null) {
            poster = settings.anonymous_poster_name;
        }
        $('#username_'+threads[index].hubName+'_' + threads[index].threadId).text(poster);
    }

    threads = []; // Empty thread cache
}


function create_hub(hubname, description, password) {
    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters: {
            hubName: hubname,
            description: description,
            password: password
        }
    }

    var request = JSON.stringify(request);

    $.ajax({
        type: 'POST',
        url: 'http://api.' + settings.apidomain + '/createHub',
        data: request,
        dataType: 'JSON'
    }).done(function (data) {
        var message = "";

        if (data['status'] == 'ok') {
            message = '<i class="uk-icon-check"></i> Hub created';
        } else if (data['status'] == 'blank') {
            message = '<i class="uk-icon-warning"></i> Field missing: ' + data['data'];
        } else if (data['status'] == 'hubExistsAlready') {
            message = '<i class="uk-icon-warning"></i> Hub exists already';
        } else if (data['status'] == 'flood') {
            message = '<i class="uk-icon-clock-o"></i> Please wait ' + Math.floor(data['data'] / 1000) + ' seconds';
        } else {
            message = '<i class="uk-icon-warning"></i>' + data['status'];
        }

        UIkit.notify({
            message: message,
            status: 'info',
            timeout: 5000,
            pos: 'top-center'
        });

        $('#navbar').show();
        $('#div_newhub').hide();
        load_hub(true);
    });
}

function delete_thread(hub, threadId, password) {

    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters: {
            hubName: hub,
            threadId: threadId,
            password: password
        }
    }

    var request = JSON.stringify(request);

    $.ajax({
        type: 'POST',
        url: 'http://api.' + settings.apidomain + '/deleteThread',
        data: request,
        dataType: 'JSON'
    }).done(function (data) {

        var message = "";
        if (data['status'] == 'ok') {
            message = '<i class="uk-icon-check"></i> Thread deleted';
            load_hub(true);
        } else if (data['status'] == 'incorrectpassword') {
            message = '<i class="uk-icon-warning"></i> Password incorrect';
        } else {
            message = '<i class="uk-icon-warning"></i> Error: ' + data['status'];
        }
        UIkit.notify({
            message: message,
            status: 'info',
            timeout: 5000,
            pos: 'top-center'
        });

    });

}

function create_thread(title, text, sign, poster) {
    // Create Thread

    // Check types in content
    var types = [];

    var links_matches = text.match(/(https?:\/\/[^\s]+)/g);
    var images_matches = text.match(/\!\[[^\]]*\]\([^\)]*\)/g);
    var video_matches = text.match(/\!video\[[^\]]*\]\([^\)]*\)/g);

    var links_count = 0;
    var images_count = 0;
    var video_count = 0;

    if (links_matches != null) {
        links_count = links_matches.length;
    }
    if (images_matches != null) {
        images_count = images_matches.length;
    }
    if (video_matches != null) {
        video_count = video_matches.length;
    }

    if (video_count != 0) {
        types.push('video');
    }
    if (images_count != 0) {
        types.push('image');
    }
    if (links_count - video_count - images_count != 0) {
        types.push('link');
    }

    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters: {
            hubName: current_hub,
            title: title,
            types: types,
            text: text,
            sign: sign,
            poster: poster
        }
    }

    var request = JSON.stringify(request);

    $.ajax({
        type: 'POST',
        url: 'http://api.' + settings.apidomain + '/createThread',
        data: request,
        dataType: 'JSON'
    }).done(function (data) {
        var message = "";
        if (data['status'] == 'ok') {
            message = '<i class="uk-icon-check"></i> Thread created';
            $("#button_cancel").click();
            // Save username and keyphrase
            localStorage.username = poster;
            if(sign != null){
                localStorage.keyphrase = sign;
            }else{
                localStorage.keyphrase = "";
            }
            load_hub(true);
        } else if (data['status'] == 'flood') {
            message = '<i class="uk-icon-clock-o"></i> Please wait ' + Math.floor(data['data'] / 1000) + ' seconds';
        } else if (data['status'] == 'blank') {
            message = '<i class="uk-icon-warning"></i> Field missing: ' + data['data'];
        } else {
            message = '<i class="uk-icon-warning"></i>' + data['status'];
        }
        UIkit.notify({
            message: message,
            status: 'info',
            timeout: 5000,
            pos: 'top-center'
        });

    });


}