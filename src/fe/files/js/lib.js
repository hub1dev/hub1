function toJSTime(stringTime) {
    return new Date(stringTime);
}

function humanTime(date) {
    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    if (Math.floor(seconds) < 0) {
        return '0 seconds';
    } else {
        return Math.floor(seconds) + " seconds";
    }
}


function timeToColor(date) {
    var seconds = Math.floor((new Date() - date) / 1000);
    var value = seconds/604800;

    if (value > 1) {
        value = 1;
    } else if (value < 0) {
        value = 0;
    }

    var hue = ((1 - value) * 120).toString(10);
    return ["hsl(", hue, ",35%,50%)"].join("");
}

function createSignatureImage(signature) {
    var seed = parseInt(signature, 16);
    // Random function
    function random() {
        var x = Math.sin(seed++) * 10000;
        return x - Math.floor(x);
    }

    var canvas = document.createElement("canvas");
    canvas.width = 12;
    canvas.height = 12;
    var ctx = canvas.getContext("2d");
    // Fill with random background
    ctx.fillStyle = '#' + Math.floor(seed * random()).toString(16).substr(0, 6);
    ctx.fillRect(0, 0, canvas.width, canvas.height)
    var operations = 10 + (Math.floor(random() * 10));
    for (i = 0; i < operations; i++) {
        type = Math.floor(random() * 3);
        switch (type) {
            case 0:
                ctx.fillStyle = '#' + Math.floor(seed * random()).toString(16).substr(0, 6);
                ctx.fillRect(Math.floor(random() * canvas.width), Math.floor(random() * canvas.height), Math.floor(random() * canvas.width), Math.floor(random() * canvas.height));
                break;
            case 1:
                ctx.strokeStyle = '#' + Math.floor(seed * random()).toString(16).substr(0, 6);
                ctx.moveTo(Math.floor(random() * canvas.width), Math.floor(random() * canvas.height));
                ctx.lineTo(Math.floor(random() * canvas.width), Math.floor(random() * canvas.height));
                ctx.stroke();
                break;
            case 2:
                ctx.strokeStyle = '#' + Math.floor(seed * random()).toString(16).substr(0, 6);
                ctx.beginPath();
                ctx.arc(Math.floor(random() * canvas.width), Math.floor(random() * canvas.height), Math.floor(random() * canvas.width), 0, 2 * Math.PI);
                ctx.stroke();
                break;
        }
    }
    return canvas.toDataURL("image/png");
}

function getHTML_threadList(data, mixedhubs, moderatorMode) {
    // Print out html
    var htmlout = '';
    for (var index in data) {
        var threadLinkIcon = '';
        var threadLink = '';
        var commentText = '';
        var commentImg = '';
        var commentLink = '';

        var in_hub = '';
        // Types
        var containslink = false;
        var containsimage = false;
        var containsvideo = false;
        for (var index_types in data[index].types) {
            if (data[index].types[index_types] == 'link') {
                //threadLinkIcon = threadLinkIcon + '<img title="This thread contains links" src="gfx/link.png"/>';
                containslink = true;
            } else if (data[index].types[index_types] == 'image') {
                //threadLinkIcon = threadLinkIcon + '<img title="This thread contains images" src="gfx/image.png"/>';
                containsimage = true;
            } else if (data[index].types[index_types] == 'video') {
                //threadLinkIcon = threadLinkIcon + '<img title="This thread contains videos" src="gfx/film.png"/>';
                containsvideo = true;
            }
        }

        var threadLinkIcon = "";
        var linkIcon = "";
        var linkTitle = "";

        if (containsvideo == true) {
			linkIcon = '<i class="uk-icon-video-camera"></i>';
            linkTitle = "This thread contains videos";
        } else if (containsimage == true) {
            linkIcon = '<i class="uk-icon-file-image-o"></i>';
            linkTitle = "This thread contains images";
        } else if (containslink == true) {
			linkIcon = '<i class="uk-icon-link"></i>';
            linkTitle = "This thread contains links";
        }else{
            linkIcon = '<i class="uk-icon-file-text-o"></i>';
            linkTitle = "This thread contains text";
		}
        threadLinkIcon = '<a id="button-preview-' + data[index].hubName + '-' + data[index].threadId + '">'+linkIcon+'</a><img id="img-preview-ok-' + data[index].hubName + '-' + data[index].threadId + '" title="Thread was loaded" src="gfx/ok.png" style="display:none"/>';

        threadLink = threadLink + '<a id="hub_link_'+data[index].hubName+'_' + data[index].threadId + '" href="http://www.' + settings.domain + '/' + data[index].hubName + '/' + data[index].threadId + '"></a>';



        // Time and username
        var jstime = toJSTime(data[index].lastComment);
        var time_humantime = humanTime(jstime);
        if (settings.timestamp_colors) {
            var time_color = timeToColor(jstime);
        } else {
            var time_color = 'hsl(0,0%,0%)';
        }

        if (data[index].commentCount != 0) {
            if (data[index].commentCount == 1) {
                commentText = '1 comment';
            }else{
                commentText = data[index].commentCount + ' comments';
            }
        }else{
            commentText = ' no comments';
        }

        if (data[index].commentCount != 0) {
            commentImg = '<i class="uk-icon-comment"/>';
        } else {
            commentImg = '<i class="uk-icon-comment-o"/>';
        }

        commentLink = '<a href="http://www.' + settings.domain + '/' + data[index].hubName + '/' + data[index].threadId + '">'+commentImg+' ' + commentText + '</a>';

        if (mixedhubs == true) {
            in_hub = 'in <a href="http://www.' + settings.domain + '/' + data[index].hubName + '"><span class="hub1-hub">' + data[index].hubName + '</span></a>';
        }

        var previewBox = '';
        /*previewBox = previewBox + ' <button id="button_preview_' + data[index].hubName + '_' + data[index].threadId + '"';
        previewBox = previewBox + 'class="uk-button uk-button-mini" type="button" title="preview this thread"><i class="uk-icon-binoculars"></i></button>';
        */

        previewBox = previewBox + '<div style="display:none;margin-top: 4px;" id="div-preview-thread-' + data[index].hubName + '-' + data[index].threadId + '" ';
        previewBox = previewBox + 'class="uk-comment-body hub1-comment-body uk-text-break"><img src="gfx/ajax-loader.gif"/></div>';


        // Add to return html
        htmlout = htmlout + '<div class="hub1-threadlist-entry">';
        // Delete Button
        if (moderatorMode == true) {
            htmlout = htmlout + '<button id="delete_thread_' + data[index].threadId + '" class="uk-button uk-button-danger uk-button-mini" type="button"><i class="uk-icon-remove"></i> </button>';
        }

        htmlout = htmlout + threadLinkIcon + ' ' + threadLink + previewBox + '<br/>';
        //htmlout = htmlout + 'by  ' + in_hub;
        htmlout = htmlout + '<h6 style="margin:0px;">' + commentLink + ' | by <span id="username_' + data[index].hubName + '_' + data[index].threadId + '" class="hub1-username"></span> '+in_hub+' | last activity <span style="color:' + time_color + ';">' + time_humantime + ' ago</span></h6>';
        //htmlout = htmlout + '<table class="threadInfoTable"><tr><td style="width:120px;"><h6>regregerge</h6></td><td style="width:120px"><h6>34534534</h6></td></tr></table>';

        htmlout = htmlout + '</div>';
    }



    return htmlout;
}


