var hubs = [];
var tutorial = 1;
var tutorial_max = 10;


$(document).ready(function () {

    $('#button_top_threads').click(function () {
        $('#div_popular_hubs').hide();
        $('#div_top_threads').show();
        $('#div_about').hide();
    });

    $('#button_active_hubs').click(function () {
        if(hubs.length == 0){
            load_hubs();
        }

        $('#div_popular_hubs').show();
        $('#div_top_threads').hide();
        $('#div_about').hide();
    });

    $('#button_about').click(function () {
        $('#div_popular_hubs').hide();
        $('#div_top_threads').hide();
        $('#div_about').show();
    });

    $('#input_searchbox').on('input', function() {
        update_hublist($('#input_searchbox').val());
        return false;
    });

    // Preview
    $(document).on('click', "[id^=button-preview-]", function () {
        var thread_id = $(this).attr('id').split('-');
        var request = {
            interfaceVersion: settings.interfaceversion,
            parameters: {
                hubName: thread_id[2],
                threadId: thread_id[3],
                getComments: 'false'
            }
        }
        var request = JSON.stringify(request);
        $('#div-preview-thread-'+ thread_id[2]+'-'+ thread_id[3]).show();
        $.ajax({
            type: 'POST',
            url: 'http://api.' + settings.apidomain + '/threadDetails',
            data: request,
            dataType: 'JSON'
        }).done(function (data) {
            if (data['status'] == 'ok') {
                $('#div-preview-thread-'+ thread_id[2]+'-'+ thread_id[3]).html(marked(data['data'].text));
                $('#button-preview-'+ thread_id[2]+'-'+ thread_id[3]).html('<i class="uk-icon-check"/>');
            }
        });
    });

    // Init
    init();
    load_top_threads();

});

function init() {
    // Set title & header
    $('#title_title').text(settings.frontpage_title);
    $("#link_header").attr('href', 'http://www.'+settings.domain);

    // show tutorial?
    if(localStorage.getItem('tutorial')==undefined){
        $('#div_tutorial').html('<a href="javascript:tutorial_next();"><img src="gfx/tutorial'+tutorial+'.png"/>');
    }
}

function tutorial_next(){
    tutorial = tutorial + 1;
    if(tutorial > tutorial_max){
        $('#div_tutorial').html('');
        localStorage.setItem('tutorial','true');
        return;
    }
    $('#div_tutorial').html('<a href="javascript:tutorial_next();"><img src="gfx/tutorial'+tutorial+'.png"/>');
}


function load_top_threads() {
    /*### Load top threads hubs ###*/

    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters: {}
    }
    var request = JSON.stringify(request);
    $.ajax({
        type: 'POST',
        url: 'http://api.' + settings.apidomain + '/topThreads',
        data: request,
        dataType: 'JSON'
    }).done(function (data) {
        if (data['status'] == 'ok') {
            var htmlOut = getHTML_threadList(data['data'], true, false);
            if(htmlOut != null){
                $('#table_top_threads').html(htmlOut);
            }

            // Fill in escaped link titles and username
            for (var index in data['data']) {

                $('#hub_link_'+data["data"][index].hubName+'_' + data["data"][index].threadId).text(data['data'][index].title);
                var poster = data['data'][index].poster;
                if (poster == null) {
                    poster = settings.anonymous_poster_name;
                }

                $('#username_'+data['data'][index].hubName+'_' + data["data"][index].threadId).text(poster);
            }
        } else {
            $('#table_popular_hubs').html('<tr><td> Error:' + data['status'] + '</td></tr>');
        }
    });
}

function load_hubs(){
    /*### Load active hubs ###*/
    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters:{
        }
    }

    var request = JSON.stringify(request);
    $.ajax({
        type: 'POST',
        url: 'http://api.'+settings.apidomain+'/getHubs',
        data : request,
        dataType: 'JSON'
    }).done(function (data) {
        if(data['status'] == 'ok') {
            hubs = data['data'];
            update_hublist('');
        }else{
            $('#table_popular_hubs').html('<tr><td> Error:'+data['status']+'</td></tr>');
        }
    });
}

function update_hublist(filter){
    var htmlout = '';
    var found = 0;
    htmlout = htmlout + '<thead><tr><td class="uk-width-3-10">Hub</td><td>Description</td><td class="uk-width-2-10">Current threads</td></tr></thead>';
    htmlout = htmlout + '<tbody>';
    for (var index in hubs){
        if(hubs[index].hubName.toLowerCase().indexOf(filter.toLowerCase()) != -1){
            var description = hubs[index].description;
            if(description.length > 64){
                description = description .substring(0,64)+'...';
            }
            htmlout = htmlout + '<tr><td><a class="hub1-hub" href="http://www.'+settings.domain+'/'+hubs[index].hubName+'">'+hubs[index].hubName+'</a></td><td>'+description+'</td><td>'+hubs[index].threadCount+'</td></tr>';
            found++;
        }

    }
    htmlout = htmlout + '</tbody>';

    console.log(found);
    if(found == 0){
        htmlout ='<tbody><tr><td>Hub "<a class="hub1-hub"><strong>'+filter+'</strong></a>" not found, click <a class="hub1-link" href="http://www.'+settings.domain+'/'+filter+'">here</a> to create it</td></tr></tbody>';
    }

    $('#table_popular_hubs').html(htmlout);
}