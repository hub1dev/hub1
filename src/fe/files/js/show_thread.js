var current_hub = "";
var current_thread = "";

var autorefresh_counter;
var autorefresh_interval;

var last_focus_margin;
var last_focus_button;

$(document).ready(function () {
    /* Main Reply Form */
    $('#input_identify').click(function () {
       if($("#input_identify")[0].checked){
           $('#div_keyphrase').show();
       }else{
           $('#div_keyphrase').hide();
       }
    });

    $('#button_submit').click(function () {
        var sign = null;
        if($('#input_identify')[0].checked){
            sign = $('#input_keyphrase').val();
        }
        create_reply([], $('#input_text').val(), sign, $('#input_poster').val(), null);
        return false;
    });

    $('#button_cancel').click(function () {
        $("#div_newcomment").hide();
        return false;
    });

    $("#button_reply").click(function () {
        // retrive username and keyphrase from localstorage
        if(localStorage.username != ""){
            $("#input_poster").val(localStorage.username)
        }else{
            $("#input_poster").val('');
        }
        if(localStorage.keyphrase != "") {
            $('#div_keyphrase').show();
            $('#input_identify').prop('checked',true);
            $('#input_keyphrase').val(localStorage.keyphrase);
        }else{
            $('#input_identify').prop('checked',false);
            $('#input_keyphrase').val('');
        }



        // Empty inputfield
        var editors = $('.CodeMirror');
        for(var x=0; x < editors.length; x++){
            editors[x].CodeMirror.setValue('');
        }

        // Close other editors
        var editors_cancelbuttons = $('[id^=button_cancel_]:visible');
        for(var x=0; x < editors_cancelbuttons.length; x++){
            editors_cancelbuttons[x].click();
        }

        // Show must be executed first, or the editors preview box is too small
        $("#div_newcomment").show();
        // Init Markdwon Editor
        $.UIkit.htmleditor($('#input_text'));
        return false;
    });

    /* Navbar */
    $("#button_autorefresh").click(function () {
        if (autorefresh_interval == null) {
            autorefresh_counter = settings.autorefresh_time;
            autorefresh_interval = window.setInterval(auto_refresh, 1000);
            $("#button_autorefresh").html('<i class="uk-icon-circle"></i> ' + autorefresh_counter);
        } else {
            window.clearInterval(autorefresh_interval);
            $("#button_autorefresh").html('<i class="uk-icon-circle-o"></i> Auto-Refresh');
            autorefresh_interval = null;
        }
        return false;
    });


    /* Comment Reply form */
    $(document).on('click', "[id^=input_identify_]", function () {
        var current_id = $(this).attr('id').split('_');
        if($('#input_identify_'+current_id[2])[0].checked){
           $('#div_keyphrase_'+current_id[2]).show();
       }else{
           $('#div_keyphrase_'+current_id[2]).hide();
       }
    });

    $(document).on('click', "[id^=button_cancel_]", function () {
        var current_id = $(this).attr('id').split('_');
        $('#div_newcomment_' + current_id[2]).hide();
        return false;
    });

    $(document).on('click', "[id^=button_reply_]", function () {
        var current_id = $(this).attr('id').split('_');
        // retrive username and keyphrase from localstorage
        if(localStorage.username != ""){
            $('#input_poster_'+current_id[2]).val(localStorage.username)
        }else{
            $('#input_poster_'+current_id[2]).val('');
        }

        if(localStorage.keyphrase != "") {
            $('#div_keyphrase_'+current_id[2]).show();
            $('#input_identify_'+current_id[2]).prop('checked',true);
            $('#input_keyphrase_'+current_id[2]).val(localStorage.keyphrase);
        }else{
            $('#input_identify_'+current_id[2]).prop('checked',false);
            $('#input_keyphrase_'+current_id[2]).val('');
        }



        // empty inputfield
        var editors = $('.CodeMirror');
        for(var x=0; x < editors.length; x++){
            editors[x].CodeMirror.setValue('');
        }

        // Close other editors
        var otherEditors = $('[id^=button_cancel_]:visible');
        for(var x=0; x < otherEditors.length; x++){
            if(otherEditors[x].id != 'button_cancel_'+current_id[2]){
                otherEditors[x].click();
            }
        }

        // Close main Editor
        $('#button_cancel').click();

        // Show must be executed first, or the editors preview box is too small
        $('#div_newcomment_' + current_id[2]).show();
        // Init Markdwon Editor
        $.UIkit.htmleditor($('#input_text_' + current_id[2]), {markdown: true, mode: 'tab', height: '200px'});
    });

    $(document).on('click', "[id^=button_submit_]", function () {
        var current_id = $(this).attr('id').split('_');
        var idChain = $('#div_idChain_' + current_id[2]).text().split(',');
        for(var index in idChain){
            idChain[index] = parseInt(idChain[index]);
        }
        var sign = null;
        if($('#input_identify_' + current_id[2])[0].checked){
            sign = $('#input_keyphrase_' + current_id[2]).val();
        }

        create_reply(idChain, $('#input_text_' + current_id[2]).val(), sign, $('#input_poster_' + current_id[2]).val(), current_id[2]);
    });

    $(document).on('click', "[id^=button_hidecomments_]", function () {
        var current_id = $(this).attr('id').split('_');

        if($('#button_hidecomments_'+current_id[2]).text() == "[-]"){
            $('#button_hidecomments_'+current_id[2]).text("[+]");
        }else{
            $('#button_hidecomments_'+current_id[2]).text("[-]");
        }

        //Hide subcomments
        $('#div_comment_li_'+current_id[2]).find('ul').first().toggle();
        $('#div_comment_body_'+current_id[2]).toggle();
    });

    // Init
    init();
    load_thread();
});

function auto_refresh() {
    autorefresh_counter = autorefresh_counter - 1;
    if (autorefresh_counter < 1) {
        $('#button_autorefresh').html('<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
        autorefresh_counter = settings.autorefresh_time + 1;
        load_thread();
    } else {
        $('#button_autorefresh').html('<i class="uk-icon-circle"></i> ' + autorefresh_counter);
    }
}

function init() {
    // Get current hub
    // TODO testurl
    if(settings.test_showthread_url != null){
        var url = settings.test_showthread_url;
    }else{
        var url = window.location.href;
    }
	url = url.replace('http://','').replace('https://','');

    var urlparts = url.split('/');
    if (urlparts.length == 3) {
        current_hub = urlparts[1];
        current_thread = urlparts[2];
    }

    // Set logo header link
    $("#link_header").attr('href', 'http://www.'+settings.domain);

    // Set breadcrumbs
    $("#a_breadcrumb_1").text('hub1');
    $("#a_breadcrumb_1").attr('href', 'http://www.' + settings.domain);
    $("#a_breadcrumb_2").text(current_hub);
    $("#a_breadcrumb_2").attr('href', 'http://www.' + settings.domain+'/'+current_hub);
    $("#a_breadcrumb_3").text(current_thread);
}

function load_thread() {
    // Load thread details
    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters: {
            hubName: current_hub,
            threadId: current_thread,
            getComments : 'true'
        }
    }

    var request = JSON.stringify(request);

    $.ajax({
        type: 'POST',
        url: 'http://api.'+settings.apidomain+'/threadDetails',
        data: request,
        dataType: 'JSON'
    }).done(function (data) {
        if (data['status'] == 'ok') {
            /* Thread */
            var poster = data["data"].poster;
            if (poster == null) {
                poster = settings.anonymous_poster_name;
            }

            var signature = '';
            if(data['data'].signature != null){
                signature = '<img title="ID: '+data['data'].signature+'" src="'+createSignatureImage(data['data'].signature)+'"/>'
            }

            /* Fill in escaped title, username and text */
            $('#title_title').text(data['data'].title);
            $('#a_title').text(data['data'].title);
            $('#a_timestamp').text(humanTime(toJSTime(data['data'].creation)));
            $('#a_signature').html(signature);
            $('#a_username').text(poster);
            $('#a_hub_link').text(current_hub);
            $('#a_hub_link').attr('href','http://www.'+settings.domain + '/' + current_hub);
            $('#div_text').html(marked(data['data'].text));

            /* Generate Comment html */
            $('#div_comments').html(generateComments(data['data'].comments, '', 0));
            // Recursively add enter-viewport-listeners to all comments that hide too long comments, generate the id image and add focus events.
            prepareComments(data['data'].comments, 0);




        } else {
            // Thread not found
            $('#title_title').text('404: Thread not found');
            $('#a_title').html('<i class="uk-icon-warning"></i> 404');
            $('#div_text').html('Thread not found, <a href="'+'http://www.'+settings.domain + '/' + current_hub+'"><i class="uk-icon-arrow-left"></i> go back to hub</a>');

            $('#div_navbar').hide();
            $('#div_comments_outer').hide();
        }
    });
}


function generateComments(comments, idChain, depth) {
    html = '';
    html = html + '<ul class="uk-comment-list" >';
    for (index in comments) {
        // Comment
        html = html + '<li id="div_comment_li_' + comments[index].commentId + '">';
        html = html + '<article id="div_comment_' + comments[index].commentId + '" class="uk-comment">';
        html = html + '<header id="div_comment_header_' + comments[index].commentId+'" class="uk-comment-header hub1-comment-header">';

        // Time
        var jstime = toJSTime(comments[index].creation);
        var time_humantime = humanTime(jstime);
        if(settings.timestamp_colors){
            var time_color = timeToColor(jstime);
        }else{
            var time_color = 'hsl(0,0%,0%)';
        }

        // Signature
        var signature = '';
        if(comments[index].signature != null){
            signature = comments[index].signature;
        }

        // Comment Header
        html = html + '<div class="uk-comment-meta"><a id="button_hidecomments_' + comments[index].commentId + '" title="Hide this comment chain">[-]</a> <span id="div_comment_signature_' + comments[index].commentId+'">'+signature+'</span> <span class="hub1-username" id="div_comment_username_' + comments[index].commentId+'"></span> | <span style="color:'+time_color+';">'+time_humantime+' ago</span> | <a class="hub1-link" title="Reply to this comment" id="button_reply_' + comments[index].commentId + '"><i class="uk-icon-comment"></i> [Reply]</a></div>'
        html = html + '</header>';
        // Comment body

        html = html + '<div id="div_comment_body_' + comments[index].commentId+'" class="uk-comment-body hub1-comment-body uk-text-break">';
        html = html + '</div>';

        /* Generate IdChain helper div */
        var idChainTemp = '';
        if(idChain.length != 0){
            idChainTemp = idChain+','+comments[index].commentId;
        }else{
            idChainTemp = comments[index].commentId;
        }
        /// /--
        var username = "";
        var keyphrase = "";
        if(localStorage.username != null){
            username = localStorage.username;
        }
        if(localStorage.keyphrase != null) {
            keyphrase = localStorage.keyphrase;
        }
        
        html = html + '<div id="div_idChain_'+comments[index].commentId+'" style="display:none;">'+idChainTemp+'</div>';
        // Hidden Reply Form
        html = html + '<div id="div_newcomment_' + comments[index].commentId + '" style="display:none">';
        html = html + '<div class="uk-panel uk-panel-box">';
        html = html + '<form id="form_newcomment_' + comments[index].commentId + '" class="uk-form uk-form-stacked">';
        html = html + '<div class="uk-form-row">';
        html = html + '<label class="uk-form-label">Name</label>';
        html = html + '<input id="input_poster_' + comments[index].commentId + '" title="The name you will be posting as" type = "text" maxlength="32" placeholder="Anonymous" value="'+username+'"/>';
        html = html + ' <input id="input_identify_' + comments[index].commentId + '" type="checkbox" title="Identify yourself within this thread"><label> Identify</label>';
        html = html + '</div>';
        html = html + '<div class="uk-form-row" id="div_keyphrase_'+comments[index].commentId+'" style="display:none;">';
        html = html + '<label class="uk-form-label">Keyphrase: </label><input id="input_keyphrase_'+comments[index].commentId+'" title="This will be used to generate your ID" type="password" maxlength="16" value="'+keyphrase+'"/>';
        html = html + '</div>';
        html = html + '<div class="uk-form-row">';
        html = html + '<label class="uk-form-label">Text</label>';
        html = html + '<textarea id="input_text_' + comments[index].commentId + '" maxlength="8192"></textarea >';
        html = html + '</div>';
        html = html + '<div id="id_area_submit' + comments[index].commentId + '" class="uk-form-row">';
        html = html + '<button id="button_submit_' + comments[index].commentId + '" class="uk-button uk-button-success" type="button"><i class="uk-icon-check"></i> Submit</button>';
        html = html + ' <button id="button_cancel_' + comments[index].commentId + '" class="uk-button uk-button-danger" type="button"><i class="uk-icon-close"></i> Cancel</button>';
        html = html + '</div>';
        html = html + '</form>';
        html = html + '</div>';
        html = html + '</div>';
        html = html + '</article>';
        //

        // Generate Subcomments
        if(comments[index].comments.length != 0){
            html = html + generateComments(comments[index].comments,idChainTemp, depth+1);
        }
        //
        html = html + '</li>';
    }

    html = html + '</ul>';


    return html;
}

function prepareComments(comments, depth) {
    for (index in comments) {
        // Fill in escaped username and text (escaped by markdown)
        var poster = comments[index].poster;
        if(poster == null){
            poster = settings.anonymous_poster_name;
        }
        $('#div_comment_username_' + comments[index].commentId).text(poster);
        $('#div_comment_body_'+comments[index].commentId).html(marked(comments[index].text));


        var elementWatcher; // temporary variable for creating elementwatchers
        // on comment enterview: add listener for render ID, add click listener, hide too long comments

        elementWatcher = scrollMonitor.create($('#div_comment_' + comments[index].commentId));
        elementWatcher.enterViewport(function() {
            var id = this.watchItem.id.split('_')[2];

            // Made IDs to pictures
            var signature = $('#div_comment_signature_' + id).text();
            if(signature.length != 0){
                $('#div_comment_signature_'+id).html('<img title="ID: '+signature+'" src="'+createSignatureImage(signature)+'"/>');
            }

            // Add focus button event
            $('#div_comment_header_'+id).on("click", function (e) {
                if(e.target.id == ''){ // No button was clicked
                    if(last_focus_button != this.id){
                        last_focus_margin = $("#div_comments").css("margin-left").replace('-','').replace('px','');
                        var new_margin = parseInt(depth)*16;
                    }else{
                        var new_margin = last_focus_margin;
                        last_focus_margin = $("#div_comments").css("margin-left").replace('-','').replace('px','');
                    }

                    $("#div_comments").css("margin-left", '-'+new_margin+'px');
                    last_focus_button = this.id;
                }
            });


            // Hide to big comments on enterviewport
            if(settings.hide_comment_treshhold_height != null){
                if($('#div_comment_body_'+id).height() > settings.hide_comment_treshhold_height){
                    var htmlout = '';
                    htmlout = htmlout + '<div id="div_hidecomment_'+id+'" style="overflow:hidden;height:'+settings.hide_comment_treshhold_height+'px;">'+$('#div_comment_body_'+id).html()+'</div>';
                    htmlout = htmlout + '<a href="javascript: $(\'#div_comment_body_'+id+'\').html($(\'#div_hidecomment_'+id+'\').html()); void(0);"><i class="uk-icon-binoculars"></i> Show more...</a>'
                    $('#div_comment_body_'+id).html(htmlout);
                }
            }

            this.destroy(); // Destroy after comment is prepared
        });

        // Recursive
        if(comments[index].comments.length != 0){
            prepareComments(comments[index].comments, depth+1);
        }
    }
    return;
}

function create_reply(subCommentsIds, text, sign, poster, formID) {
    // Post comment
    var request = {
        interfaceVersion: settings.interfaceversion,
        parameters: {
            hubName: current_hub,
            threadId: current_thread,
            text: text,
            sign: sign,
            poster: poster,
            subComments: subCommentsIds
        }
    }

    var request = JSON.stringify(request);

    $.ajax({
        type: 'POST',
        url: 'http://api.'+settings.apidomain+'/createComment',
        data: request,
        dataType: 'JSON'
    }).done(function (data) {

        var message = "";

        if (data['status'] == 'ok') {
            message = '<i class="uk-icon-check"></i> Comment posted';
            if(formID == null){
                $("#button_cancel").click(); // Hide reply form
            }else{
                $("#button_cancel_"+formID).click(); // Hide reply form
            }
            // Save username and keyphrase
            localStorage.username = poster;
            if(sign != null){
                localStorage.keyphrase = sign;
            }else{
                localStorage.keyphrase = "";
            }

            load_thread();
        } else if(data['status']=='flood'){
            message = '<i class="uk-icon-clock-o"></i> Please wait '+Math.floor(data['data']/1000)+' seconds';
        }else if(data['status']=='blank'){
            message = '<i class="uk-icon-warning"></i> Field missing: '+data['data'];
        } else {
            message = '<i class="uk-icon-warning"></i>'+data['status'];
        }

        UIkit.notify({
            message: message,
            status: 'info',
            timeout: 5000,
            pos: 'top-center'
        });

    });


}